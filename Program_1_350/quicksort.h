/****************************************************************
 * 'quicksort.h'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 03-25-2018--14:14:50
 *
 * Pseudocode From Book:
 * algorithm quicksort(A, lo, hi) is
 *     if lo < hi then -- look at this line think of the inverse logic of x<y the opposite is X >= y
 *         p := partition(A, lo, hi)
 *         quicksort(A, lo, p) -- look at this line its just one number off
 *         quicksort(A, p + 1, hi)
 *
 * algorithm partition(A, lo, hi) is
 *     pivot := A[lo] -- look here at this line in your corresponding code... this is looking for an array at index 
 *     i := lo - 1
 *     j := hi + 1
 *     loop forever -- look here at this line youll see it 
 *         do
 *             i := i + 1
 *         while A[i] < pivot
 *
 *         do
 *             j := j - 1
 *         while A[j] > pivot
 *
 *         if i >= j then
 *             return j
 *
 *         swap A[i] with A[j]
**/

#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <algorithm>

using std::swap;

//note returns INDEX of median
template<typename T> inline
int medianOf3(T A[], int l, int r){
	 //this is overcommented... also, try and avoid using pointers
	T* a = A + l;//array name is just pointer to 1st (0 index) elem., + l shifts l*(T size)
	T* b = A + l + (r-l)/2;//middle item... int division rounds down
	T* c = A + r;

	//when a is a pointer, *a is the dereference operator (gives value a points to)
	T* m;
	if(*a < *b){
		if(*b < *c) m=b; 
		else if(*c < *a) m=a;
		else m=c;
	} else{ //b <=a
		if(*a < *c) m=a;
		else if(*c < *b) m=b;
		else m=c;
	}
	return m-A; //m-A is the number of elements from A[0]

}

//remember: l and r are INLCUSIVE (just like Lomuto)
template<typename T>
int hoarePartition( T A[], int l, int r ) {
	T pivot = A[medianOf3(A,l,r)];
	int i = (l - 1), j = (r + 1); // index of +/- middle value
	while( true ) { //inf loop to rearranged
		do i++; while ( A[i] < pivot );
		do j--; while ( A[j] > pivot );
		if( i >= j ) return j;
		swap( A[i], A[j] );
	}//end while
	return 0;
}//end hoarePartition

template<typename T>
void quicksort( T A[], int l, int r ) { //helper function
	if( l >= r ) return;
	T pivot = hoarePartition(A, l, r);
	quicksort( A, l, pivot );
	quicksort( A, pivot+1, r );
}//end quicksort

#endif

/****************************************************************
 * End 'quicksort.h'
**/